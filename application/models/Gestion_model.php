<?php
class Gestion_model extends MY_Model
{
    protected $table = 'Gestion';

    public function __construct()
    {
        parent::__construct();
	}
	
	public function getMatrimonio($dpi = '')
	{
		if($dpi == ''){
			return array();
		}
		return $this->db->select('g.id_gestion as nomatrimonio, g.dpihombre, h.nombres as nombrehombre, h.apellidos as apellidohombre, g.dpimujer, m.nombres as nombremujer, m.apellidos as apellidomujer, g.fecha',false)
						->from($this->table.' g')
						->join('Persona h','g.dpihombre = h.dpi')
						->join('Persona m','g.dpimujer = m.dpi')
						->where('g.tipo_gestion_id',1)
						->where("( g.dpihombre = $dpi OR g.dpimujer = $dpi)")
						->order_by('g.id_gestion','desc')
						->get()
						->row();
	}
}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
