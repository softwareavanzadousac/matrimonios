<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Matrimonio extends REST_Controller {

	function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Content-Type: application/json; charset=utf-8");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->load->model('persona_model','persona');
		$this->load->model('gestion_model','gestion');
	}

	public function setMatrimonio_post()
	{   
		$this->crudLibraries();
		// $_POST = json_decode(file_get_contents("php://input"), true);
		$this->form_validation->set_rules('dpihombre', 'DPI del Hombre', 'trim|required');
		$this->form_validation->set_rules('dpimujer', 'DPI de la Mujer', 'trim|required');
		$this->form_validation->set_rules('fecha', 'Fecha de Matrimonio', 'trim|required');
		if ($this->form_validation->run() === false) {
			return $this->response( [
				'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
				'mensaje' => validation_errors()
			], REST_Controller::HTTP_OK );
        }else{
			$hombre = $this->persona->getSoltero($this->input->post('dpihombre'));
			$mujer = $this->persona->getSoltero($this->input->post('dpimujer'));
			if(!isset($hombre->idPersona)){
				return $this->response( [
					'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
					'mensaje' => 'Hombre ya se encuentra casado'
				], REST_Controller::HTTP_OK );

			}else if(!isset($mujer->idPersona)){
				return $this->response( [
					'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
					'mensaje' => 'Mujer ya se encuentra casada'
				], REST_Controller::HTTP_OK );
			}
			$this->persona->update(['estadocivil' => 'C'],$hombre->idPersona,'idPersona');
			$this->persona->update(['estadocivil' => 'C'],$mujer->idPersona,'idPersona');
			$this->gestion->insert(
				array(
					'tipo_gestion_id' => 1,
					'dpihombre' => $hombre->dpi,
					'dpimujer' => $mujer->dpi,
					'fecha' => 	$this->input->post('fecha'),
				)
			);
            return $this->response([
				'estado' => REST_Controller::HTTP_OK,
				'mensaje' => 'Matrimonio realizado exitosamente',
			], REST_Controller::HTTP_OK);
		}
	}

	
	public function getMatrimonio_post()
	{   
		$this->crudLibraries();
		// $_POST = json_decode(file_get_contents("php://input"), true);
		$this->form_validation->set_rules('dpi', 'DPI', 'trim|required');
		if ($this->form_validation->run() === false) {
			return $this->response( [
				'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
				'mensaje' => validation_errors()
			], REST_Controller::HTTP_OK );
		}else{
			$matrimonio = $this->gestion->getMatrimonio($this->input->post('dpi'));
			if(empty($matrimonio)){
				return $this->response(['estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'respuesta' => $matrimonio],REST_Controller::HTTP_OK);
			}
			return $this->response($matrimonio,REST_Controller::HTTP_OK);
		}
	}

    private function crudLibraries()
    {
        // load classes
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
    }

}
