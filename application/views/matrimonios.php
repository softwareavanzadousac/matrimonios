<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Matrimonios</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- Main row -->
		<div class="row">
			<div class="card-body">
				<div class="row">
					<div class="mensaje col-md-12">

					</div>
					<div class="col-md-12">
						<select name="" id="url">
							<option value="http://matrimonios.local/" <?php echo (ENVIRONMENT == 'development')?'selected="selected"':''?>>local</option>
							<option value="http://35.193.113.191" <?php echo (ENVIRONMENT == 'production')?'selected="selected"':''?>>produccion</option>
							<option value="">Grupo 1</option>
							<option value="http://35.239.54.7">Grupo 2</option>
							<option value="http://35.184.97.83">Grupo 3</option>
							<option value="http://35.193.113.191">Grupo 4</option>
							<option value="">Grupo 5</option>
							<option value="http://35.232.40.193">Grupo 6</option>
						</select>
					</div>
					<div class="col-md-4">
						<!-- select -->
						<div class="form-group">
							<label for="dpihombre">Hombre</label>
							<select class="form-control" id="hombres">
								<option value="">Seleccione Uno</option>
							</select>
							<input type="text" class="form-control" name="dpihombre" id="dpihombre">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="dpimujer">Mujer</label>
							<select class="form-control" id="mujeres">
								<option value="">Seleccione Uno</option>
							</select>
							<input type="text" class="form-control" name="dpimujer" id="dpimujer">
						</div>
					</div>
					<div class="col-md-4">
						<label for="fecha">Fecha</label>
						<input type="date" class="form-control"  name="fecha" id="fecha">
					</div>
				</div>
				<div class="row">
					<a href="javascript:guardarMatrimonio();" class="btn btn-success">Guardar</a>
					<!-- <input type="submit" class="btn btn-success" value="Guardar"> -->
				</div>
			</div>
		</div>
		<!-- /.row (main row) -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
	var url = '';
	$(function(){
		url = $('#url').val();
		llenarHombres();
		llenarMujeres();
	});
	$('#url').change(function () {
		url = this.value;
		// console.log(url);
	});
	$('#hombres').change(function () {
		$('#dpihombre').val(this.value);
	});
	$('#mujeres').change(function () {
		$('#dpimujer').val(this.value);
	});
	function llenarHombres() {
		$.ajax({
			url: url+':9001/api/persona/getPersonas',
			data: {genero: 'M', estado_civil:'S', difunto: 0},
			success: function (response) {
				$('#hombres').empty();
				$('#hombres').append(new Option("Seleccione Uno",""));
				$.each(response,function (index, value) {
					$('#hombres').append(new Option(value.nombre,value.dpi));
				});
			}
		});
	}
	function llenarMujeres() {
		$.ajax({
			url: url+':9001/api/persona/getPersonas',
			data: {genero: 'F', estado_civil:'S', difunto: 0},
			success: function (response) {
				$('#mujeres').empty();
				$('#mujeres').append(new Option("Seleccione Uno",""));
				$.each(response,function (index, value) {
					$('#mujeres').append(new Option(value.nombre,value.dpi));
				});
			}
		});
	}

	function guardarMatrimonio() {
		var dpihombre = $('#dpihombre').val();
		var dpimujer = $('#dpimujer').val();
		var fecha = $('#fecha').val();
		if(url != ''){
			$.ajax({
				url: 'http://35.193.113.191:9001/setMatrimonio',
				type: "POST",
				dataType : "json",
				data: {
						dpihombre 	: dpihombre,
						dpimujer	: dpimujer,
						fecha		: fecha
				}
			})
			.done(function (data,textStatus,jqXHR) {
				setMsj(data);
			});
		/*	$.ajax({
				url: url+':10000/post/comunicacionesb',
				type: "POST",
				dataType : "json",
				data: {
					url	 	: url+':9001/setMatrimonio',
					tipo 		: 'post',
					parametros: {
						dpihombre 	: dpihombre,
						dpimujer	: dpimujer,
						fecha		: fecha
					}
				}
			})
			.done(function (data,textStatus,jqXHR) {
				setMsj(data);
			});
		*/
		}
	}
	function setMsj(response) {
		var innerHtml = "";
		if(response.status != 200){
			innerHtml 		+= "<div class='alert alert-success alert-dismissible fade show' role='alert'>";
			innerHtml 		+= response.mensaje;
			innerHtml 		+= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
			innerHtml 		+= "</div>";
			$('#dpihombre, #dpimujer').val('');
			llenarHombres();
			llenarMujeres();
		}else{
			innerHtml 		+= "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
			innerHtml 		+= response.mensaje;
			innerHtml 		+= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
			innerHtml 		+= "</div>";
		}
		$('.mensaje').html(innerHtml);
	}
</script>
